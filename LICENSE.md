			   IRE'S PUBLIC LICENSE (IPL)
			  Version 1.1.3, January 2019

Copyright (C) 2019 Ire <chaffe@tfwno.gf>

This license is licensed by the Ire's Public License (IPL).

			   IRE'S PUBLIC LICENSE (IPL)
	TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You may copy and distribute this work freely, granted that you also
     provide the original license for any verbatim copies.

  1. The author is not liable for any damages caused directly or indirectly
     from the work. That is to say, no warranty is given or implied for it.

  2. You are free to modify this work. If you choose to distribute this
     modification, you must change the name of the project so it remains
     identifiable from the original work.

  3. Derivative projects do not necessarily need to implement the IPL, but
     they must have some method of allowing the public to audit the project.
     It is recommended, but not necessary, to use a license that enforces this.

  4. Do whatever you like with this license, but any modified versions of this
     license may not be called the "IRE'S PUBLIC LICENSE". 
